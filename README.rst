What is it?
===========

An Django app that should be able to:

- create an odt document from a template using **unotools** from https://bitbucket.org/t2y/unotools

- serving this files (pdf, odt)




How to install?
===============

1) Create a virtual environment

    $ mkvirtualenv -p /usr/bin/python3.4 --system-site-packages odtmanagement

2) Create a project directory and clone repository

    $ mkdir odtmanagement

    $ cd odtmanagement

    $ git clone "repository"

2) Install requirements:

    $ pip install -r requirements/base.txt

3) Launch the Server

    $ ./manage.py runserver

How to use?
===========

Starting Libre Office silently
------------------------------

    $ soffice --accept='socket,host=localhost,port=8100;urp;StarOffice.Service' --invisible

Testing
-------
In module testing, a function allows testing odt generation.


Requirements
============

unotools
--------

    $ sudo aptitude install -y libreoffice libreoffice-script-provider-python uno-libs3 python3-uno python3