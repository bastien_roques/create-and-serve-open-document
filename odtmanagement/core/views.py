from django.views.generic import ListView
from .models import Contact


class IndexView(ListView):
    template_name = 'core/contact_list.html'
    model = Contact
