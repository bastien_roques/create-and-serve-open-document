from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=100, db_column='nom')
    firstname = models.CharField(max_length=100, blank=True)

    def _get_full_name(self):
        return '{} {}'.format(self.name, self.firstname)
    full_name = property(_get_full_name)

    def __str__(self):
        return self.full_name
