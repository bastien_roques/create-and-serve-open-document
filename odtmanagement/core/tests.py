from .utils import ContactDocument


def generate_unodocument():

    doc = ContactDocument(1)
    table_name = "table_test"
    data = (
        ('1value1', 'value2', 'value3', 'value4'),
        ('2value1', 'value2', 'value3', 'value4'),
        ('3value1', 'value2', 'value3', 'value4'),
        ('4value1', 'value2', 'value3', 'value4'),
    )
    doc.fill_table(data=data, table_name=table_name)

    return doc
