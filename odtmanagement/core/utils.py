__author__ = 'bastien'

from os.path import join

from unotools.unohelper import convert_path_to_url
from unotools import Socket, connect
from unotools.component.writer import Writer

from core.models import Contact
from com.sun.star.awt.FontWeight import NORMAL


def insert_text(cursor, cell, string, style=NORMAL):
    cursor.CharWeight = style
    cell.insertString(cursor, string, False)
    cursor.CharWeight = NORMAL


class UnoDocument(Writer):

    template_url = ''
    file_name = ''
    path_file = ''
    fields_mapping = ()

    def __init__(self, host='localhost', port='8100', template_url=None):
        context = connect(Socket(host, port))
        url = convert_path_to_url(template_url)
        super().__init__(context, url)

    def generate_mapping(self):
        get_by_name = self.raw.TextFieldMasters.getByName
        field_master = 'com.sun.star.text.fieldmaster.User.{}'
        for field, value in self.fields_mapping:
            get_by_name(field_master.format(field)).Content = value

    def fill_table(self, data, table_name):
        table = self.raw.TextTables.getByName(table_name)
        table.Rows.insertByIndex(1, len(data)-1)
        for row_i, row in enumerate(data, start=1):
            for column, value in enumerate(row):
                table.getCellByPosition(column, row_i).setString(str(value))

    def save_odt(self):
        url = join(self.path_file, '{}.odt'.format(self.file_name))
        url = convert_path_to_url(url)
        self.store_to_url(url, 'FilterName', 'writer8')

    def save_pdf(self):
        url = join(self.path_file, '{}.pdf'.format(self.file_name))
        url = convert_path_to_url(url)
        self.store_to_url(url, 'FilterName', 'writer_pdf_Export')


class ContactDocument(UnoDocument):

    template_url = 'templates/documents/first_template.ott'
    path_file = 'templates/documents/'

    def __init__(self, contact_pk):
        super().__init__(template_url=self.template_url)
        contact = Contact.objects.get(pk=contact_pk)
        self.file_name = contact.full_name
        self.fields_mapping = (
            ('user_name', contact.name),
            ('user_firstname', contact.firstname),
        )
        self.generate_mapping()
