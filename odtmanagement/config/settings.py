"""
Django settings for pyrdt project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/


For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""


from os.path import basename
from unipath import Path
from configurations import Configuration, values


class Common(Configuration):
    """
    """
    BASE_DIR = Path(__file__).ancestor(2)
    DJANGO_ROOT = Path(__file__).ancestor(1)
    SITE_NAME = basename(DJANGO_ROOT)

    SECRET_KEY = 'CHANGE ME !!!'

    DEBUG = True
    TEMPLATE_DEBUG = DEBUG

    ALLOWED_HOSTS = ['*']

    ADMINS = (
        ('bastien', 'bastien.roques11@gmail.com'),
    )

    DJANGO_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.humanize',
    )

    LOCAL_APPS = (
        'core',
    )

    INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS

    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )

    ROOT_URLCONF = 'config.urls'

    WSGI_APPLICATION = 'config.wsgi.application'

    LANGUAGE_CODE = 'fr-FR'

    TIME_ZONE = 'UTC'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )

    STATIC_ROOT = DJANGO_ROOT.child("static")
    STATIC_URL = '/static/'
    STATICFILES_DIRS = (
        BASE_DIR.child("static"),
    )

    TEMPLATE_DIRS = (
        BASE_DIR.child("templates"),
    )

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )

    WSGI_APPLICATION = 'config.wsgi.application'

    INTERNAL_IPS = ('127.0.0.1',)

    LOGIN_REDIRECT_URL = '/'

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
        ]
    }

    VAT = float(20)


class Dev(Common):
    """
    The in-development settings and the default configuration.
    """

    SECRET_KEY = 'p!)5f+0d15nd0uaj%nv-9a54bl-w7dcd!)eyq542a5c*)kmkr0'

    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'odtmanagement',
            }
    }


class Prod(Common):
    """
    The in-production settings.
    """
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    ALLOWED_HOSTS = ['*']

    DEBUG = False
    TEMPLATE_DEBUG = DEBUG
    # Empty by design to trigger a warning to fill it with something sensible.
    SECRET_KEY = values.SecretValue
