from django.conf.urls import patterns, include, url
from core.views import IndexView

from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('', url(r'^$', IndexView.as_view(), name='home'),
                       url(r'^core/', include('core.urls', namespace='core')),
                       )

urlpatterns += patterns('', url(r'^admin/', include(admin.site.urls)),
                        )
